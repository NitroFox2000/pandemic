from django.db import models
from django.contrib.auth.models import User


# Мед/Мат
class Module(models.Model):
    title = models.CharField(max_length=50, verbose_name='Модуль')
    description = models.TextField(blank=True, verbose_name='Описание')
    photo = models.ImageField(upload_to='photos/%Y/%m/%d/', verbose_name='Фото', blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Модуль'  # Наименование в ед ч
        verbose_name_plural = 'Модули'  # Наименование в мн ч
        ordering = ['title']


# Предметы/врачи
class Category(models.Model):
    title = models.CharField(max_length=100, verbose_name='Категория')
    description = models.TextField(blank=True, verbose_name='Описание')
    content = models.TextField(blank=True, verbose_name='Контент')
    photo = models.ImageField(upload_to='photos/%Y/%m/%d/', verbose_name='Фото', blank=True)
    module = models.ForeignKey('Module', on_delete=models.PROTECT, null=True, verbose_name='Модуль')
    redactor = models.ForeignKey(User, on_delete=models.PROTECT, null=True, verbose_name='Редактор категории')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Категория'  # Наименование в ед ч
        verbose_name_plural = 'Категории'  # Наименование в мн ч
        ordering = ['-title']


# Темы/болезни
class Component(models.Model):
    title = models.CharField(max_length=30, verbose_name='Наименование')
    description = models.TextField(blank=True, verbose_name='Описание')
    content = models.TextField(blank=True, verbose_name='Контент')
    code = models.CharField(max_length=30, verbose_name='Код')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Последнее обновление')
    photo = models.ImageField(upload_to='photos/%Y/%m/%d/', verbose_name='Фото', blank=True)
    is_published = models.BooleanField(default=True, verbose_name='Пубикация')
    category = models.ForeignKey('Category', on_delete=models.PROTECT, null=True, verbose_name='Категория')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Тема'  # Наименование в ед ч
        verbose_name_plural = 'Темы'  # Наименование в мн ч
        ordering = ['-created_at']


# Подболезнь
class Paragraph(models.Model):
    title = models.CharField(max_length=30, verbose_name='Параграф')
    description = models.TextField(blank=True, verbose_name='Описание')
    content = models.TextField(blank=True, verbose_name='Контент')
    code = models.CharField(max_length=30, verbose_name='Код')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Последнее обновление')
    photo = models.ImageField(upload_to='photos/%Y/%m/%d/', verbose_name='Фото', blank=True)
    is_published = models.BooleanField(default=True, verbose_name='Пубикация')
    component = models.ForeignKey('Component', on_delete=models.PROTECT, null=True, verbose_name='Категория')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Параграф'  # Наименование в ед ч
        verbose_name_plural = 'Параграфы'  # Наименование в мн ч
        ordering = ['-created_at']


# Часть
class Part(models.Model):
    title = models.CharField(max_length=30, verbose_name='Часть')
    description = models.TextField(blank=True, verbose_name='Описание')
    content = models.TextField(blank=True, verbose_name='Контент')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Последнее обновление')
    photo = models.ImageField(upload_to='photos/%Y/%m/%d/', verbose_name='Фото', blank=True)
    is_published = models.BooleanField(default=True, verbose_name='Пубикация')
    paragraph = models.ForeignKey('Paragraph', on_delete=models.PROTECT, null=True, verbose_name='Категория')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Часть'  # Наименование в ед ч
        verbose_name_plural = 'Части'  # Наименование в мн ч
        ordering = ['-created_at']
