from django.urls import path
from .views import *

urlpatterns = [
    path('', index),
    path('med/', med),
    path('mat/', mat),
    path('cen/', cen),
    path('cont/', cont)
]
