from django.contrib import admin

# Register your models here.
from .models import *


admin.site.register(Module)
admin.site.register(Category)
admin.site.register(Component)
admin.site.register(Paragraph)
admin.site.register(Part)
