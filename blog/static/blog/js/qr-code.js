// https://chart.googleapis.com/chart?cht=qr&chld=H&chs=400x400&chl=%D0%BA%D0%B0%D1%88%D0%B0
// L - [Default] Allows recovery of up to 7% data loss
// M - Allows recovery of up to 15% data loss
// Q - Allows recovery of up to 25% data loss
// H - Allows recovery of up to 30% data loss
// https://developers.google.com/chart/infographics/docs/qr_codes
$(function() {
  $('#do').click(function() {
    $('#dsp').attr('src',
      'https://chart.googleapis.com/chart?cht=qr&chld=H&chs=400x400&chl=' +
      encodeURIComponent($('#txt').val())
    );
    $('#dt').text($('#txt').val());
  });
  $('#clean').click(function() {
    $('#txt').val('');
  });
});
