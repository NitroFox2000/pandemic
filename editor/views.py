from django.shortcuts import render


# Create your views here.

def index(request):
    context = {
        'title': 'Главная',
    }
    return render(request, 'editor/index2.html', context)


def index2(request):
    context = {
        'title': 'Заголовок',
    }
    return render(request, 'editor/index2.html', context)
