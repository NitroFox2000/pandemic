$(function() {
  // такая вот защита от тупых ботов. пока работает без сбоев.
  $('#form-one').empty().html(
    '<form action="/bin/png/favicon.ico" method="POST"' +
    ' enctype="multipart/form-data" target="small_frame">' +
    '<input type="file" name="file">' +
    '<input type="submit" value="Перобразовать">' +
    '</form>' +
    '<iframe name="small_frame" id="small_frame"' +
    ' frameborder="no" height="1" width="1"' +
    ' src="about:blank"></iframe>'
  );
  $('#form-many').empty().html(
    ['<form action="/bin/png/favicon.ico" method="POST"' +
     ' enctype="multipart/form-data" target="small_frame">' +
     '<input type="file" name="file1">',
     '<input type="file" name="file2">',
     '<input type="file" name="file3">',
     '<input type="file" name="file4">',
     '<input type="file" name="file5">',
     '<input type="file" name="file6">',
     '<input type="file" name="file7">',
     '<input type="file" name="file8">',
     '<input type="file" name="file9">',
     '<input type="file" name="file10">',
     '<input type="submit" value="Собрать иконку">' +
     '</form>'].join('<br>')
  );
  $('#small_frame').hide();
});
