$(function() {
  function d(s, act) {
    try {
      return act(s);
    } catch(e) {
      if (e.message) {
        return e.message;
      }
    }
    return 'Hm.. error.'
  }
  var dec = $('#dec'), enc = $('#enc');
  $('#b-dec').click(function(e) {
    e.preventDefault();
    dec.val(d(enc.val(), decodeURIComponent));
  });
  $('#b-enc').click(function(e) {
    e.preventDefault();
    enc.val(d(dec.val(), encodeURIComponent));
  });
  $('#b-clean').click(function(e) {
    e.preventDefault();
    enc.val('');
    dec.val('');
  });
});
