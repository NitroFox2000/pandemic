/*jslint maxerr:3 white:true sloppy:true nomen:true vars:true */
/*global $, google, window, document */
function init_map() {
  var GM = google.maps;
  var _a = new GM.LatLng(55.7559, 37.7006);
  var map = new GM.Map(
    document.getElementById('map_canvas'),
    {
      zoom: 10,
      center: _a,
      mapTypeId: GM.MapTypeId.ROADMAP
    }
  );
  var mrk = new GM.Marker({
    title: "You here",
    clickable: false,
    draggable: true,
    position: _a,
    map: map
  });
  var update_info = function() {
    var x = mrk.getPosition();
    var a = x.lat();
    var b = x.lng();
    document.getElementById('info').innerHTML = a + ', ' + b;
//    $('#info').text(a + ', ' + b);
  };
  GM.event.addListener(mrk, 'dragend', update_info);
  GM.event.addListener(mrk, 'drag', update_info);
  GM.event.addListener(map, 'click', function(e) {
    var p = e.latLng;
    mrk.setPosition(p);
    update_info();
  });
  update_info();
}

google.maps.event.addDomListener(window, 'load', init_map);
