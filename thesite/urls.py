"""thesite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# from django.contrib import admin
# from django.urls import path, include

# import editor
# from editor import urls
# from main import urls

# import main
# from thesite import settings

# urlpatterns = [
#     path('admin/', admin.site.urls),
#     path('editor/', include(editor.urls)),
#     path('', include(main.urls))
# ]
from django.contrib import admin
from django.conf.urls import url, include

urlpatterns = [
    url('admin/', admin.site.urls),
    url('editor/', include('editor.urls')),
    url('', include('main.urls')),
    url('blog/', include('blog.urls')),
]