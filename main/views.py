from django.shortcuts import render
from django.http import HttpResponse


# Create your views here.

def index(request):
    return render(request, 'main/index.html')


def med(request):
    return render(request, 'main/med.html')


def mat(request):
    return render(request, 'main/mat.html')

def cen(request):
    return render(request, 'main/cen.html')

def cont(request):
    return render(request, 'main/cont.html')